import unittest

from brute_force import brute_force_solution
from reduce_rework import reduce_rework_solution
from reduce_rows import reduce_rows_solution


class SolutionTestCase(unittest.TestCase):

    def __small1(self, solution_fn):
        self.assertEqual(2, solution_fn([[0, 0, 0, 0],
                                         [0, 1, 0, 0],
                                         [1, 0, 1, 1]]))

    def __small2(self, solution_fn):
        self.assertEqual(4, solution_fn([[0, 1, 0, 1],
                                         [1, 0, 1, 0],
                                         [0, 1, 0, 1],
                                         [1, 0, 1, 0]]))

    def __one_row(self, solution_fn):
        self.assertEqual(1, solution_fn([[0, 1, 0, 1]]))
        self.assertEqual(1, solution_fn([[0, 0, 0, 1]]))
        self.assertEqual(1, solution_fn([[0, 0, 0, 0]]))
        self.assertEqual(1, solution_fn([[1, 1, 1, 1]]))


    def test_small1_bf(self):
        self.__small1(brute_force_solution)

    def test_small2_bf(self):
        self.__small2(brute_force_solution)

    def test_one_row_bf(self):
        self.__one_row(brute_force_solution)

    def test_small1_rr(self):
        self.__small1(reduce_rework_solution)

    def test_small2_rr(self):
        self.__small2(reduce_rework_solution)

    def test_one_row_rr(self):
        self.__one_row(reduce_rework_solution)

    def test_small1_redrow(self):
        self.__small1(reduce_rows_solution)

    def test_small2_redrow(self):
        self.__small2(reduce_rows_solution)

    def test_one_row_redrow(self):
        self.__one_row(reduce_rows_solution)


if __name__ == '__main__':
    unittest.main()
