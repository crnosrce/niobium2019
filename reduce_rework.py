import itertools


def copy2D(A):
    return [row.copy() for row in A]


def allSame(row):
    first = row[0]
    for val in row[1:]:
        if val != first:
            return False
    return True


def countSameRows(A):
    count = 0
    for row in A:
        if allSame(row):
            count += 1
    return count


def flipColumn(flippedA, colIdx):
    for row in flippedA:
        row[colIdx] = 1 if row[colIdx] == 0 else 0


def flipColumns(A, indicesToFlip):
    flippedA = copy2D(A)
    for colIdx in indicesToFlip:
        flipColumn(flippedA, colIdx)
    return flippedA


def search_solution_recursive(indices, previously_flipped_result,
                              previously_flipped_columns, previous_max,
                              max_switches):
    len_prev = len(previously_flipped_columns)
    max_same = previous_max
    if len_prev < max_switches:
        numSwitches = len_prev + 1
        for indexCombo in itertools.combinations(indices, numSwitches):
            sortedIndexCombo = sorted(indexCombo)
            if sortedIndexCombo[:len_prev] == previously_flipped_columns:
                # we are just flipping one new column compared with the previously_flipped_result
                new_flipped_result = copy2D(previously_flipped_result)
                flipColumn(new_flipped_result, sortedIndexCombo[-1])
                max_same = max(max_same,
                               countSameRows(new_flipped_result),
                               search_solution_recursive(indices, new_flipped_result,
                                                         sortedIndexCombo, max_same,
                                                         max_switches))
    return max_same


def reduce_rework_solution(A):
    numCols = len(A[0])
    MAX_SWITCHES = (numCols + 1) // 2
    indices = [i for i in range(numCols)]
    maxIdenticalRows = countSameRows(A)  # the baseline - no flips
    maxIdenticalRows = max(maxIdenticalRows,
                           search_solution_recursive(indices, A, [], maxIdenticalRows, MAX_SWITCHES))
    return maxIdenticalRows


def solution(A):
    return reduce_rework_solution(A)
