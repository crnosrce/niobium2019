
def rowsInverted(first_row, compare_row):
    for idx in range(len(first_row)):
        if first_row[idx] == compare_row[idx]:
            return False
    return True


def sameOrInverted(first_row, compare_row):
    return first_row == compare_row or rowsInverted(first_row, compare_row)


def countSameOrInverted(A, remaining_indices):
    first_index = remaining_indices[0]
    first_row = A[first_index]
    remaining_indices = remaining_indices[1:]
    residual_indices = []
    count = 1  # first row compared against the others, so at least 1!
    for idx in remaining_indices:
        if sameOrInverted(first_row, A[idx]):
            count += 1
        else:
            residual_indices.append(idx)
    return count, residual_indices


def reduce_rows_solution(A):
    num_rows = len(A)
    indices = [x for x in range(num_rows)]
    maxIdenticalRows = 1  # we can always get at least 1 row the same
    while len(indices) >= 2:  # we need 2 rows to be able to compare them
        # comparing the first remaining row to the other rows
        # they only way they could end up the all the same
        # after any number of swaps is if the rows are identical
        # to start with or complete opposites
        count, residual_indices = countSameOrInverted(A, indices)
        maxIdenticalRows = max(maxIdenticalRows, count)
        indices = residual_indices
    return maxIdenticalRows


def solution(A):
    return reduce_rows_solution(A)
