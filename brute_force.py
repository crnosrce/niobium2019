import itertools


def allSame(row):
    first = row[0]
    for val in row[1:]:
        if val != first:
            return False
    return True


def countSameRows(A):
    count = 0
    for row in A:
        if allSame(row):
            count += 1
    return count


def flipColumn(flippedA, colIdx):
    for row in flippedA:
        row[colIdx] = 1 if row[colIdx] == 0 else 0


def flipColumns(A, indicesToFlip):
    flippedA = [row.copy() for row in A]
    for colIdx in indicesToFlip:
        flipColumn(flippedA, colIdx)
    return flippedA


def brute_force_solution(A):
    numRows = len(A)
    numCols = len(A[0])
    MAX_SWITCHES = (numCols + 1) // 2
    indices = [i for i in range(numCols)]
    maxIdenticalRows = 0
    for numSwitches in range(MAX_SWITCHES + 1):
        for indexCombo in itertools.combinations(indices, numSwitches):
            flippedA = flipColumns(A, indexCombo)
            identicalRows = countSameRows(flippedA)
            maxIdenticalRows = max(maxIdenticalRows, identicalRows)
            if maxIdenticalRows == numRows:
                return maxIdenticalRows  # can't do better than this!
    return maxIdenticalRows


def solution(A):
    return brute_force_solution(A)
